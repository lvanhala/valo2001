ajax = (url, data, type = 'GET', dataType = 'json', timeout = undefined) ->
  settings = { url: url, data: data, type: type, dataType: dataType, contentType: 'application/json;charset=utf-8', timeout: timeout}
  Bacon.fromPromise($.ajax(settings))

get = (url) -> ajax(url)

post = (url, data) -> ajax(url, JSON.stringify(data), 'POST')

eventTarget = (event) -> $(event.currentTarget)

api =
  getScenes: -> get("/api/scenes").map(".scenes")
  getState: -> get("/api/state").map(".state")
  selectScene: (id) -> post("/api/scenes/select?id=#{id}").map(".state")
  setAlarm: (data) -> post("/api/alarms", data).map(".state")

ui =
  list: -> $(".scene-list")

  setScenes: (scenes) ->
    list = @list().empty()
    itemHeight = $(window).height() / scenes.length

    createRow = (scene) ->
      li = $("<li/>").attr("data-id", scene.id)
      li.height(itemHeight).css("line-height", itemHeight + "px")
      liContent = $("<span/>", class: "scroll-container")
      li.append(liContent)
      liContent.append(leftPage(scene)).append(rightPage(scene))
      li

    leftPage = (scene) ->
      sceneName = $("<span/>").text(scene.name)
      sceneTimerInfo = $("<span/>", class: "alarm-info").hide()
      $("<span/>", class: "page").append(sceneTimerInfo).append(sceneName)

    rightPage = (scene) ->
      timer = $("<span/>", class: "timer off")
      timerStatus = $("<span/>", class: "timer-status").text("Ajastin: ")
      timerInput = $("<input/>", type: "time")
      timer.append(timerStatus).append(timerInput)
      $("<span/>", class: "page").append(timer)

    for scene in scenes
      list.append(createRow(scene))

  setActiveScene: (sceneId) ->
    $(".scene-list li").removeClass("selected")
    $(".scene-list li[data-id=#{sceneId}]").addClass("selected")
    ui.scrollElementTo($(".scene-list li:not([data-id=#{sceneId}]) .scroll-container"), "0px")

  updateAlarms: (alarms) ->
    $(".alarm-info").text("")
    $("input").val("00:00")

    for alarm in alarms
      li = $("li[data-id=#{alarm.sceneId}]")
      li.find(".alarm-info").text(alarm.alarmTime ? "").toggle(alarm.alarmEnabled).toggleClass("invalid", not alarm.isValid)
      li.find(".timer").toggleClass("off", not alarm.alarmEnabled)
      li.find("input").val((alarm.alarmTime ? "00:00").replace(".",":")).toggleClass("invalid", not alarm.isValid)

  scrollElementTo: (el, x) ->
    el.css("transition": "all 0.3s")
    el.css("transform": "translate3d(#{x},0px,0px)")

  getTranslateX: (el) ->
    text = el.css("transform")
    if text.indexOf("matrix") is 0
      parseInt(text.substring(7, text.length-1).split(",")[4])
    else
      0

  handleSwipe: (list) ->
    start = list.asEventStream("pointerdown", ".scroll-container")
    move = list.asEventStream("pointermove", ".scroll-container")
    end = list.asEventStream("pointerup pointerleave", ".scroll-container")

    scrollPos = start.map((e) ->
      target = $(e.currentTarget)
      target.css("transition": "none")
      startX: e.originalEvent.x, startY: e.originalEvent.y, el: target, initialTranslate: ui.getTranslateX(target)
    ).flatMapLatest((startPos) ->
      move.takeUntil(end).map((e) -> $.extend({}, startPos, { currentX: e.originalEvent.x, currentY: e.originalEvent.y })).merge(Bacon.once(null))
    ).toProperty()

    wasMoved = (val) -> val isnt null

    scrollPos.filter(wasMoved).onValue (event) ->
      dx = event.currentX - event.startX + event.initialTranslate
      event.el.css("transform": "translate3d(#{dx}px,0px,0px)")

    start.flatMapLatest(-> end.take(1)).map(scrollPos).filter(wasMoved).onValue (e) ->
      dx = e.currentX - e.startX

      newX = if dx < -100
        "-50%"
      else if dx > 100
        "0px"
      else
        e.initialTranslate + "px"

      ui.scrollElementTo(e.el, newX)

alarmUpdateStream = ->
  timerData = (timer) ->
    alarmEnabled: not timer.hasClass("off")
    sceneId: timer.closest("li").attr("data-id")
    alarmTime: timer.find("input").val()

  getTimer = (e) -> eventTarget(e).closest(".timer")

  timerClicks = ui.list().asEventStream("click", ".timer-status").map(getTimer)
    .doAction((timer) -> timer.toggleClass("off"))
    .map(timerData)

  timerInput = ui.list().asEventStream("change", "input").map(getTimer).map(timerData)
  timerInput.merge(timerClicks)

app =
  init: (scenes) ->
    ui.setScenes(scenes)
    ui.handleSwipe(ui.list())

    state = new Bacon.Bus()

    alarmResponses = alarmUpdateStream().flatMapLatest (data) ->
      api.setAlarm(
        sceneId: data.sceneId
        alarmTime: data.alarmTime
        alarmEnabled: data.alarmEnabled
      )

    sceneSelects = ui.list().asEventStream("pointerup", "li .page:first-child").doAction(".preventDefault").map(eventTarget).map((item) -> item.closest("li").attr("data-id"))
    sceneChanges = state.toProperty().map(".activeScene").sampledBy(sceneSelects, (a,b) -> {oldScene:a, newScene:b}).filter((scenes) -> scenes.oldScene isnt scenes.newScene).map(".newScene")
    sceneResponses = sceneChanges.flatMapLatest(api.selectScene).doAction(ui.resetScrolls)

    state.plug(Bacon.mergeAll(sceneResponses, api.getState(), alarmResponses, getLiveUpdates()))

    state.onValue (state) ->
      ui.setActiveScene(state.activeScene)
      ui.updateAlarms(state.alarms)

getLiveUpdates = ->
  bus = new Bacon.Bus()
  socket = io.connect("/")
  socket.on("state", bus.push)
  bus.map(".state")

$ ->
  api.getScenes().onValue(app.init)
express = require("express")
app = express()
server = require('http').createServer(app)
io = require('socket.io').listen(server)
_ = require("underscore")
Q = require("q")
Bacon = require("baconjs")

baconMongo = require("./async-mongo")
settings = require("./settings")
util = require("./util")

hue = require("./hue")
zwave = require("./zwave")

defaultState =
  activeScene: 0
  alarms: {}

init = ->
  withDb = ->
    if settings.mongodb?
      baconMongo.connect(settings.mongodb, "state").doAction -> console.log("Connected to MongoDB.")
    else
      console.log("MongoDB address not specified. Not saving changes to db.")
      Bacon.once(null)

  loadState = ->
    withDb().flatMap((coll) ->
      results = coll?.find() ? Bacon.once(null)
      results.flatMap (arr) ->
        Bacon.once
          state: _.first(arr) ? _.extend({}, defaultState)
          collection: coll
    )

  initDevices = ->
    Bacon.combineTemplate
      hue: hue(settings.hue)
      zwave: zwave(settings.zwave)

  Bacon.combineTemplate(
    state: loadState()
    devices: initDevices()
  ).map((data) ->
    _.extend({}, data.state, devices: data.devices)
  )

init().onValue (context) ->
  coll = context.collection
  state = context.state
  console.log("Loaded state:", state)

  updateState = (updateFunc) ->
    updateFunc(state)
    io.sockets.emit("state", util.formatStateJson(state))
    if coll?
      util.saveToDb(coll, state).onValue (obj) -> console.log("Saved to db: ", obj)

  activateScene = (id) ->
    scene = settings.scenes[id]
    for deviceId, command of scene.devices
      deviceType = settings.devices[deviceId].type
      device = context.devices[deviceType]
      
      if device?
        device.act(settings.devices[deviceId].config, command)
      else
        console.log("Device type #{deviceType} not installed")

  startTimer = (state) ->
    Bacon.interval(1000).map(util.currentTimeString).skipDuplicates().onValue (currentTime) ->
      for id, alarm of state.alarms
        if alarm.alarmTime is currentTime and alarm.alarmEnabled
          console.log("Alarm!", alarm)
          updateState (s) ->
            s.activeScene = alarm.sceneId        
          activateScene(alarm.sceneId)

  app.use(express.static(__dirname + "/../site"))
  app.use(express.bodyParser())

  app.get("/api/scenes", (req, res) ->
    res.send({scenes: settings.scenes})
  )

  app.get("/api/state", (req, res) ->
    res.send(util.formatStateJson(state))
  )

  app.post("/api/scenes/select", (req, res) ->
    updateState (s) ->
      s.activeScene = req.query.id
    activateScene(req.query.id)
    res.send(util.formatStateJson(state))
  )

  app.post("/api/alarms", (req, res) ->
    parsedTime = util.parseTime(req.body.alarmTime)
    newAlarm =
      sceneId: req.body.sceneId
      alarmTime: parsedTime ? req.body.alarmTime
      alarmEnabled: req.body.alarmEnabled
      isValid: parsedTime?

    updateState (s) ->
      s.alarms[req.body.sceneId] = newAlarm
    res.send util.formatStateJson(state)
  )

  server = server.listen(8000, ->
    console.log("Listening on port %d", server.address().port)
    startTimer(state)
  )

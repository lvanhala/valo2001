Bacon = require("baconjs")
request = require("request")

baconRequest = (settings) ->
	Bacon.fromBinder (sink) ->
		request(settings, (error, response, body) ->
			if error?
				sink(new Bacon.Error("http request failed to error #{error} with settings " + JSON.stringify(settings)))
			else if response.statusCode isnt 200
				sink(new Bacon.Error("http request returned #{response.statusCode} with settings " + JSON.stringify(settings)))
			else
				sink(body)
			sink(new Bacon.End())
		)
		->

module.exports =
	get: (url) ->
		baconRequest(url)

	put: (url, body) ->
		baconRequest
			url: url,
			body: JSON.stringify(body),
			method: "PUT"
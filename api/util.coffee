_ = require("underscore")

padding = (i) ->
  s = i.toString()
  if s.length < 2 then "0" + s else s

module.exports =
  parseTime: (timeString) ->
    groups = /(^\d{1,2})([:\.](\d\d))?$/.exec(timeString.trim())
    if groups?
      hour = parseInt(groups[1])
      min = if groups[3]? then parseInt(groups[3]) else 0
      "#{padding(hour)}.#{padding(min)}"

  currentTimeString: ->
    time = new Date()
    padding(time.getHours()) + "." + padding(time.getMinutes())

  formatStateJson: (state) ->
    state:
      activeScene: state.activeScene
      alarms: _.values(state.alarms)

  saveToDb: (coll, obj) ->
    coll.remove().flatMap -> coll.insert(obj)

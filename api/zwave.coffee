Bacon = require("baconjs")
bHttp = require("./baconhttp")
dns = require("dns")

zwave = (settings, ip) ->
	url = (id, toggle) -> "http://#{ip}:8083/ZWaveAPI/Run/devices[#{id}].instances[0].commandClasses[0x25].Set(#{toggle})"
	
	toggleLight = (id, isOn) ->
		bHttp.get(url(id, if isOn is "on" then 1 else 0))
			.onError (error) -> console.log("zwave request failed:", error)

	act: (config, command) ->
		if command is "on" or command is "off"
			toggleLight(config.id, command)
		else
			throw new Error("value not supported")

module.exports = (settings) ->
	bus = new Bacon.Bus()
	console.log("Looking up zWave api ip:", settings.api)

	dns.lookup(settings.api, (err, address) ->
		throw err if err?
		console.log("Found zWave api at", address)
		bus.push(zwave(settings, address))
	)
	bus.take(1)
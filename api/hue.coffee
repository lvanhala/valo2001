bHttp = require("./baconhttp")

getHueIp = ->
	bHttp.get("http://www.meethue.com/api/nupnp").map (body) ->
		JSON.parse(body)[0].internalipaddress
			
hue = (settings, ip) ->
	url = (cmd) -> "http://#{ip}/api/#{settings.user}/#{cmd}"
	
	setBrightness = (id, brightness) ->
		bHttp.put(url("lights/#{id}/state"),
			bri: brightness
			on: true
		).onError (error) -> console.log("Hue request failed:", error)

	toggleLight = (id, isOn) ->
		response = bHttp.put(url("lights/#{id}/state"),
			on: if isOn is "on" then true else false
		).onError (error) -> console.log("Hue request failed:", error)

	act: (config, command) ->
		action = ->
			if command is "on" or command is "off"
				toggleLight(config.id, command)
				toggleLight(config.id, command) 
			else
				setBrightness(config.id, Number(command))
		action()
		# Do it twice just to be sure! Loving Hue <3
		setTimeout(action, 1000)

module.exports = (settings) ->
	getHueIp().map (ip) -> hue(settings, ip)
MongoClient = require("mongodb").MongoClient
_ = require("underscore")
Bacon = require("baconjs")

collection = (col) ->
  wrapCursonFn = (name) -> ->
    cursor = col[name].apply(col, arguments)
    Bacon.fromNodeCallback(cursor, 'toArray')

  wrapFn = (name) -> ->
    Bacon.fromNodeCallback.apply(Bacon, [col, name].concat(argsToArray(arguments)))

  argsToArray = (args) ->
    Array.prototype.slice.call(args, 0)

  nonCursorWrapper = _.chain(['findOne', 'insert', 'update', 'remove', 'save', 'count', 'aggregate', 'findAndModify'])
    .map((name) -> [name, wrapFn(name)])
    .object()
    .value()

  cursorWrapper = _.chain(['find'])
    .map((name) -> [name, wrapCursonFn(name)])
    .object()
    .value()

  _.extend({}, nonCursorWrapper, cursorWrapper)

module.exports =
  connect: (db, collectionName) ->
    Bacon.fromNodeCallback(MongoClient.connect, db).map (db) ->
      collection(db.collection(collectionName))

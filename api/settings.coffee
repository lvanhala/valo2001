module.exports =
  mongodb: null # mongodb url

  hue:
    user: "defaultuser"

  zwave:
    api: "pi.local" # hostname for raspberry pi

  devices: 
    "ikea": { type: "zwave", config: {id:2} },
    "block": { type: "zwave", config: {id:3} },
    "2": { type: "hue", config: { id: 1} },
    "3": { type: "hue", config: { id: 2} },
    "4": { type: "hue", config: { id: 3} }

  scenes:
    [
      {
        id: 0, 
        name: "Kaikki päällä",
        devices:
          "ikea": "on"
          "block": "on",
          "2": 255,
          "3": 255,
          "4": 255
      },
      {
        id: 1,
        name: "Leffa",
        devices:
          "ikea": "off",
          "block": "on",
          "2": 128,
          "3": 128,
          "4": 128
      },
      {
        id: 2, 
        name: "Kaikki pois",
        devices:
          "ikea": "off",
          "2": "off",
          "3": "off",
          "4": "off",
      }
    ]